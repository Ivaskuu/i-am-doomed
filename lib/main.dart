import 'package:flutter/material.dart';
import 'ui_game.dart';

void main()
{
  runApp(new MyApp());
}

class MyApp extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    return new MaterialApp
    (
      title: 'Get me out',
      theme: new ThemeData(primarySwatch: Colors.brown),
      home: new UIGame()
    );
  }
}